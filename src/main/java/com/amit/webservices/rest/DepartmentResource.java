package com.amit.webservices.rest;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.amit.webservices.domain.Department;
import com.amit.webservices.exception.NotFoundException;
import com.amit.webservices.service.DepartmentService;

@RestController
public class DepartmentResource {

	@Autowired
	private DepartmentService departmentService;

	@GetMapping("/department")
	public List<Department> findAll() {
		return departmentService.findAll();
	}

	@GetMapping("/department/{depId}")
	public Department findById(@PathVariable Integer depId) {
		Optional<Department> optionalDep = departmentService.findById(depId);
		if (!optionalDep.isPresent()) {
			throw new NotFoundException("Department not found for id: " + depId);
		}
		Department department = optionalDep.get();
		return department;
	}

	@PostMapping("/department")
	public ResponseEntity<Department> save(@Validated @RequestBody Department department) {
		Department savedDep = departmentService.save(department);

		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{depId}")
				.buildAndExpand(savedDep.getId()).toUri();

		return ResponseEntity.created(location).build();
	}

	@DeleteMapping("/department/{depId}")
	public void deleteById(@PathVariable Integer depId) {
		departmentService.deleteById(depId);
	}
}
