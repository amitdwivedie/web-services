package com.amit.webservices.rest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.amit.webservices.domain.HelloWorld;

@RestController()
public class HelloWorldResource {

	@GetMapping("hello-world")
	public String helloWorld() {
		return "Hello world example";
	}

	@GetMapping("hello-world-bean")
	public HelloWorld helloWorldBean() {
		return new HelloWorld("This is a bean class of HelloWorld");
	}

	@GetMapping("path/{name}")
	public HelloWorld pathVariable(@PathVariable String name) {
		return new HelloWorld(String.format("Path value is %s", name));
	}

}
