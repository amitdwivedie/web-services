package com.amit.webservices.rest;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.amit.webservices.domain.Employee;
import com.amit.webservices.exception.NotFoundException;
import com.amit.webservices.service.EmployeeService;

@RestController
public class EmployeeResource {

	@Autowired
	private EmployeeService employeeService;

	@GetMapping("/employee")
	public List<Employee> findAll() {
		return employeeService.findAll();
	}

	@GetMapping("/employee/{empId}")
	public Employee findById(@PathVariable Integer empId) {
		Optional<Employee> optionalEmp = employeeService.findById(empId);
		if (!optionalEmp.isPresent()) {
			throw new NotFoundException("Employee not found for id: " + empId);
		}
		Employee employee = optionalEmp.get();
		return employee;
	}

	@PostMapping("/employee")
	public ResponseEntity<Employee> save(@Validated @RequestBody Employee employee) {
		Employee savedEmp = employeeService.save(employee);

		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{empId}")
				.buildAndExpand(savedEmp.getId()).toUri();

		return ResponseEntity.created(location).build();
	}

	@DeleteMapping("/employee/{empId}")
	public void deleteById(@PathVariable Integer empId) {
		employeeService.deleteById(empId);
	}
}
