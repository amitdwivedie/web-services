package com.amit.webservices.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController()
public class I18NResource {

	@Autowired
	private MessageSource messageSource;

	@GetMapping("i18n/message")
	//@RequestHeader(name = "Accept-Language", required = false) Locale locale
	public String i18nExample() {
		return messageSource.getMessage("hello.world.message", null, LocaleContextHolder.getLocale());
	}
}
