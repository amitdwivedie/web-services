package com.amit.webservices.rest;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.amit.webservices.domain.DynamicFilteringBean;
import com.amit.webservices.domain.StaticFilteringBean;
import com.fasterxml.jackson.databind.ser.FilterProvider;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;

@RestController
public class FieldsFilteringResource {

	@GetMapping("filter/static/bean")
	public StaticFilteringBean getStaticFilterBean() {
		return new StaticFilteringBean("val1", "val2", "val3", "val4");
	}

	@GetMapping("filter/static/beans")
	public List<StaticFilteringBean> getStaticFilterBeans() {
		List<StaticFilteringBean> beans = new ArrayList<>();
		beans.add(new StaticFilteringBean("val01", "val02", "val03", "val04"));
		beans.add(new StaticFilteringBean("val11", "val12", "val13", "val14"));
		return beans;
	}

	@GetMapping("filter/dynamic/bean")
	public MappingJacksonValue getDynamicFilterBean() {
		DynamicFilteringBean dynamicFilteringBean = new DynamicFilteringBean("val1", "val2", "val3", "val4");

		return getDynamicMappedFields(dynamicFilteringBean, "field1", "field4");
	}

	@GetMapping("filter/dynamic/beans")
	public MappingJacksonValue getDynamicFilterBeans() {
		List<DynamicFilteringBean> beans = new ArrayList<>();
		beans.add(new DynamicFilteringBean("val01", "val02", "val03", "val04"));
		beans.add(new DynamicFilteringBean("val11", "val12", "val13", "val14"));

		return getDynamicMappedFields(beans, "field1", "field3", "field4");
	}

	private MappingJacksonValue getDynamicMappedFields(Object bean, String... fields) {
		MappingJacksonValue mapping = new MappingJacksonValue(bean);
		SimpleBeanPropertyFilter filter = SimpleBeanPropertyFilter.filterOutAllExcept(fields);
		FilterProvider filters = new SimpleFilterProvider().addFilter("dynamicBeanFilter", filter);
		mapping.setFilters(filters);

		return mapping;
	}
}
