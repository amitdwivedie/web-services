package com.amit.webservices.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.amit.webservices.domain.Employee;

public interface EmployeeRepository extends JpaRepository<Employee, Integer>{

}
