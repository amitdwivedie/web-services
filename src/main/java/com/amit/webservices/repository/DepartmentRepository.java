package com.amit.webservices.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.amit.webservices.domain.Department;

public interface DepartmentRepository extends JpaRepository<Department, Integer>{

}
