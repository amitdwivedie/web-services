package com.amit.webservices.service;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amit.webservices.domain.Department;
import com.amit.webservices.repository.DepartmentRepository;

@Service
public class DepartmentService {

	private Logger log = LoggerFactory.getLogger(getClass());

	@Autowired
	private DepartmentRepository departmentRepository;

	public List<Department> findAll() {
		return departmentRepository.findAll();
	}

	public Optional<Department> findById(Integer depId) {
		log.debug("Find department of Id: " + depId);
		return departmentRepository.findById(depId);
	}

	public void deleteById(Integer depId) {
		log.debug("Delete department of Id: " + depId);
		departmentRepository.deleteById(depId);
	}

	public Department save(Department department) {
		log.debug("Save department: " + department);
		return departmentRepository.save(department);
	}

}
