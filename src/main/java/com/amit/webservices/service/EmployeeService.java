package com.amit.webservices.service;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amit.webservices.domain.Employee;
import com.amit.webservices.repository.EmployeeRepository;

@Service
public class EmployeeService {

	private Logger log = LoggerFactory.getLogger(getClass());

	@Autowired
	private EmployeeRepository employeeRepository;

	public List<Employee> findAll() {
		return employeeRepository.findAll();
	}

	public Optional<Employee> findById(Integer empId) {
		log.debug("Find employee of Id: " + empId);
		return employeeRepository.findById(empId);
	}

	public void deleteById(Integer empId) {
		log.debug("Delete employee of Id: " + empId);
		employeeRepository.deleteById(empId);
	}

	public Employee save(Employee employee) {
		log.debug("Save employee: " + employee);
		return employeeRepository.save(employee);
	}
}
