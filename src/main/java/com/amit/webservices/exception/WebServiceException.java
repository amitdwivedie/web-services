package com.amit.webservices.exception;

import java.util.Date;

public class WebServiceException {

	private Date timestamp;
	private String message;
	private String details;

	public WebServiceException(Date timestamp, String message, String details) {
		super();
		this.timestamp = timestamp;
		this.message = message;
		this.details = details;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public String getMessage() {
		return message;
	}

	public String getDetails() {
		return details;
	}

	@Override
	public String toString() {
		return "WebServiceException [timestamp=" + timestamp + ", message=" + message + ", details=" + details + "]";
	}

}
