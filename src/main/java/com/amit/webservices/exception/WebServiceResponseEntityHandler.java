package com.amit.webservices.exception;

import java.util.Date;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class WebServiceResponseEntityHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler(Exception.class)
	public final ResponseEntity<WebServiceException> handleAllException(Exception ex, WebRequest request)
			throws Exception {

		WebServiceException exception = new WebServiceException(new Date(), ex.getMessage(),
				request.getDescription(false));

		return new ResponseEntity<>(exception, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ExceptionHandler(NotFoundException.class)
	public final ResponseEntity<WebServiceException> handleEmployeeNotFoundException(NotFoundException ex,
			WebRequest request) throws Exception {

		WebServiceException exception = new WebServiceException(new Date(), ex.getMessage(),
				request.getDescription(false));

		return new ResponseEntity<>(exception, HttpStatus.NOT_FOUND);
	}
	
	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(
			MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
		WebServiceException exception = new WebServiceException(new Date(), "Validation failed",
				ex.getBindingResult().toString());
		return new ResponseEntity<>(exception, HttpStatus.BAD_REQUEST);
	}
}
